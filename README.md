# How to run the VRP model

## Software & Versions
- Minizinc 2.5.1
- Gecode Solver 6.3.0

## Step to run
- Open Project file vrp.mzp in the main directory
- Set Gecode 6.3.0 as solver
- Go to "Show configuration editor" > Options > Enable Time limit with value 300
- Click on Run and choose data file


## Author
- Mario Sessa (@kode-git)
